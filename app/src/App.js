import React, {Component} from 'react';
import DrawerSimpleExample from './Components/Drawer';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'; // add
import LoginForm from './Components/LoginForm';
import Leads from './Components/Lead/leads';
import Grid from '@material-ui/core/Grid';
import logo from './logo.svg';
import './App.css';
import { AuthProvider, AuthConsumer } from './Components/Context/AuthContext';
import {
  BrowserRouter as Router,
  Route,
  Redirect
} from "react-router-dom";


export default class App extends Component {

  render() {
    return (
      <Router>
<AuthProvider>

<MuiThemeProvider>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">JunkTeam Login</h1>
          </header>
          <Grid width={5} gap={16}>
        <div className="Menu-Styles">
          <br/>
          <DrawerSimpleExample/>
        </div>
        <br/>
     
    
        <Route exact path="/" component={LoginForm}  />

        <PrivateRoute path="/leads" component={Leads} />

            
              
          </Grid>    
        </div>
      
      </MuiThemeProvider>
      </AuthProvider>
      </Router>
      
    );
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
<AuthConsumer>
  {({ isAuth }) => (
    <Route
      render={
        props =>
          isAuth 
          ? <Component {...props} />
          : <Redirect to="/" />
      }
      {...rest}
    />
  )}
  </AuthConsumer>
);


