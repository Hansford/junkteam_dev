
import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { AuthConsumer } from './Context/AuthContext';
import { Link, Redirect } from 'react-router-dom';


export default class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = ({
            user: {
                username: '',
                password: ''               }
            }
        ); 

        this.handleSubmit = this.handleSubmit.bind(this);
        
      
    }
  
    handleChange(propertyName, event) {
        const user = this.state.user;
        user[propertyName] = event.target.value;
        this.setState({ user: user });
    }

    handleSubmit(event) {
        console.log(this.state.user.username)
        let headers = new Headers();
        headers.append('Authorization', 'Basic ' + btoa(this.state.user.username + ":" + this.state.user.password));
        fetch('http://localhost:5000/api/tokens', {
            method: 'POST',
            headers: headers      
        })
        .then(response => response.json())
        .then(data => this.onSetToken(data));
        
        event.preventDefault();
        
        
  } 

    onSetToken = (data) => {
        if (data.token) { 
            localStorage.setItem('token', JSON.stringify(data.token).slice(1, -1));
            this.setState({user : {isAuthenticated : true }});
            this.setAuth();} else { 
            console.log("Invalid Credentials!");
            console.log(this.state.user.isAuthenticated)
        };
        return null;
  }   
    setAuth = () =>
    <AuthConsumer>
        {console.log("Setting Auth")}
    {({ isAuth, login }) => (
      <div >
       {isAuth ? (
          <ul>

            <Redirect  to="/leads" />
              
            
          </ul>
        ) : (
          {login},
          console.log(isAuth),

          <Redirect to="/leads" />
        )}
      </div>
    )}
  </AuthConsumer>
 
    render() {
         return (
     
       <div>   
            <form onSubmit={this.handleSubmit}>
                <label> Username: 
                    <TextField
                        required
                        id="required"
                        margin="normal"
                        value={this.state.user.username}
                        onChange={this.handleChange.bind(this, 'username')}
                    />
                </label>
        <br/>
                <label> Password:
                    <TextField
                        id="password-input"
                        label="Password"
                        //className={classes.textField}
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        value={this.state.user.password}
                        onChange={this.handleChange.bind(this, 'password')}
                    />
                </label>
              
           <br/>
           
                    <RaisedButton
                        label="Login"
                        type="submit"
                        value="Submit"
                       
                    />
              
              </form>
              <AuthConsumer>
              {({ isAuth, login, logout }) => (
                <div >
                  <h3>
                    <Link  to="/">
                      HOME
                    </Link>
                  </h3>
        
                  {isAuth ? (
                   
                      <Redirect to="/leads"/> 
                    
                  ) : (
                    <button onClick={login}>login</button>
                  )}
                </div>
              )}
            </AuthConsumer>
       </div>
          
         
        );
    }
}