    
import React from 'react';
import axios from 'axios';

const API = 'http://localhost:5000/api/leads';

export default class Leads extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            leads : [],
            isLoading: true,
            error: null
            };
        /* left as example (reminder) for binding methods in constructor
        this.handleSubmit = this.handleSubmit.bind(this);
        */
    }

    componentDidMount(){
 
      this.intervalId = setInterval(() => this.loadData(), 15000);
      this.loadData(); // also load one immediately
    
      }
    componentWillUnmount() {
      clearInterval(this.intervalId);
    }

  loadData() {
    
      
  axios.get(API, { headers: { Authorization: `Bearer ${localStorage.getItem('token')}` } })
  .then(result => this.setState({
  leads: result.data.items,
  isLoading: false
  
}))

.catch(error => this.setState({
  error,
  isLoading: false
}));
console.log("updated")
}
    render() {
        const { leads, isLoading, error } = this.state;
    
        if (error) {
          return <p>Error: {error.message}</p>;
        }
    
        if (isLoading) {
          return <p>Loading ...</p>;
        }
    
        return (
      <div>
        <h1>Appointments:</h1>
          <ul>
            
            {leads.map(lead =>
              <li key={lead.lead_id}>
                <p>Name: {lead.name} {lead.lastname} <br/>
                   Email: {lead.email} <br/>
                   Address: {lead.address} <br/>
                   Phone #:	{lead.phone_number} <br/>
                   Requested Date: {lead.date} <br/>	
                   Time: {lead.time} <br/>
                   Description:	{lead.description} <br/> 
                   Est. Vol: {lead.est_volume} <br/>	
                   Cost: {lead.cost} <br/>
                   
                </p>
            
              </li>
            )}
          </ul>
        
           </div>
        );
      }
}