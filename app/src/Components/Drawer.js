import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';




export default class DrawerSimpleExample extends React.Component {

    constructor(props) {
      super(props);
      this.state = {open: false};
    }
  
    handleToggle = () => this.setState({open: !this.state.open});
  
    render() {
      return (
        <div>
           <RaisedButton
          id="menu-button"
          label="Menu"
          onClick={this.handleToggle}
        />
         
          
          <Drawer open={this.state.open}>
          
          <MenuItem> <AppBar
        color="inherit"
        iconElementLeft={<h1>Menu</h1>}
        iconElementRight={<IconButton onClick={this.handleToggle}>
          <FontIcon className="material-icons">clear</FontIcon>
      </IconButton>}
      /></MenuItem>
        <List>
          <MenuItem><ListItem primaryText="Inbox" leftIcon={<ContentInbox />} /></MenuItem>
          <MenuItem> <ListItem primaryText="Starred" leftIcon={<ActionGrade />} /></MenuItem>
          <MenuItem><ListItem primaryText="Sent mail" leftIcon={<ContentSend />} /></MenuItem>
          <MenuItem> <ListItem primaryText="Drafts" leftIcon={<ContentDrafts />} /></MenuItem>
          <MenuItem><ListItem primaryText="Inbox" leftIcon={<ContentInbox />} /></MenuItem>
        </List>
 
        <MenuItem>
        <RaisedButton label="Material UI" />
        </MenuItem>
        <MenuItem><br></br></MenuItem>

          </Drawer>
        </div>
      );
    }
  }