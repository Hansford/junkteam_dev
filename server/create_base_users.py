#!/usr/bin/env python
"""Create a initial admin user"""
from getpass import getpass
from junkteam import db, app
from junkteam.models import User, UserRoles, Role, Item
import sys

def create_roles():
    try:
        admin = Role(name="ADMIN")
        manager = Role(name="MANAGER")
        tech = Role(name="TECH")

        role_list = (admin, manager, tech)
        for role in role_list:
            db.session.add(role)
            print(f"Added Role: {role}")
            db.session.commit()
        return True
    except:
        print("Failure: Roles not created.")
        return False


def add_Role(user, role):
    try:
        print(f"Role added: id = {role}")
        print(f"Username:{user.username}")
        user_role = UserRoles(user_id=user.id, role_id=role)
        return user_role
    except:
        return False

def add_AdminRole(user):
    print("Adding Admin Role")
    try:
        admin = add_Role(user, 1)
        db.session.add(admin)
        db.session.commit()
        print(f"Assigned User: {user.username} Role: Admin ")
        return True
    except:
        print("Role Fail")
        return False

def add_ManagerRole(user):
    print("Adding Manager Role")
    try:
        manager = add_Role(user, 2)
        db.session.add(manager)
        db.session.commit()
        print(f"Assigned User: {user.username} Role: Manager ")
        return True
    except:
        print("Role Fail")
        return False

def add_TechRole(user):
    try:
        tech = add_Role(user, 3)
        db.session.add(tech)
        db.session.commit()
        print(f"Assigned User: {user.username} Role: Technician ")
        return True
    except:
        print("Role Fail")
        return False


def create_user(username, email, password):
    print("creating user...")
    '''
    if User.query.all():
        create = input('A user already exists! Create another? (y/n):')
        if create == 'n':
            print("Goodbye!")
            return
    '''
    if not username:
        username = input(f"Enter {role} Username:")
        email = input(f"Enter {role} Email Address:")
        password = getpass()
        assert password == getpass('Password (again):')
        user.set_password(password)
    username = username
    email = email
    password = password 
    
    #Try to create the user
    try:
        user = User(username=username, email=email)
        db.session.add(user)
        user.set_password(password)
        db.session.commit()
        print("User Created")
        return user
    

#try to assign a role
   #try:
   #     addRole()
 
    except:
        print("Failure: User not created.")
        return False

def create_admin(username, email, password):
    try:
        user = create_user(username, email, password)
        print (f"Admin create with username: {username}")
        add_AdminRole(user)
        print("Role Pass")
        return True

    except:
        print("Failure: Admin creation failed.")
        return False
def create_manager(username, email, password):
    try:
        user = create_user(username, email, password)
        print (f"Manager create with username: {username}")
        add_ManagerRole(user)
        print("Role Pass")
        return True
    except:
        return False

def create_technician(username, email, password):
    try:
        print("Creating Technician")
        user = create_user(username, email, password)
        print (f"Tech create with username: {username}")
        add_TechRole(user)
        print("PASS: Role Technician Added to {user.username}")
        return True
    except:
        return False

def create_item(name, desc, cost, lookup):

    try:
        print(f"Creating Item:{name}")
        item = Item(name=name, description=desc, cost=cost, lookup=lookup)
        db.session.add(item)
        db.session.commit()
        print(f"Item {item.name} Created")
        
        return True
    except:

        return False


def main():
    with app.app_context():
        db.metadata.create_all(db.engine)
        create_roles()
        
        #Try to create Base Users
        try:
            
            create_admin("admin", "admin@junkteam.ca", "password")
            print("Admin Created")
            create_manager("manager", "manager@junkteam.ca", "password")
            print("Manager Created")
            create_technician("technician", "technician@junkteam.ca", "password")
            print("Technician Created")
            print("PASS: BASE USERS CREATED")
            print("Creating Items...")
            create_item("Minimum Truck Load", "", 75.00, 'Min' )
            create_item("1/8 Truck Load", "Approx. half a Pick-Up Truck", 120.00, '1/8' ) #cost should be float
            create_item("1/6 Truck Load", "Approx. 3 quarters of a Pick-Up Truck", 160.00, '1/6' ) 
            create_item("1/4 Truck Load", "Approx. 1 and 1 quarter Pick-Up Trucks", 210.00, '1/4' ) 
            create_item("Half Truck Load", "Approx. 2 and a half Pick-Up Trucks", 315.00, '1/2' ) 
            create_item("3/4 Truck Load", "Approx. 3 and 3 quarter Pick-Up Trucks", 400.00, '3/4' )  
            create_item("Full Truck Load", "Approx. 5 Pick-Up Trucks", 465.00, '1' )
            create_item("Demolition", "", 300.00, "Dem" )
            create_item("+ 1/2 Hour Labour", "Exceeded Alotted Time", 50.00, 'Labour') 
            print("Created Items")
            

        except:
            print("Epic Fail")
            sys.exit(1)

if __name__ == '__main__':
    sys.exit(main())
