from flask import jsonify, request, url_for
from junkteam.models import Quote
from junkteam.api import bp
from junkteam.api.errors import bad_request
from junkteam import db, app
from junkteam.api.auth import token_auth

@bp.route('/quotes', methods=['GET'])
@token_auth.login_required
def get_quotes():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 3, type=int), 100)
    data = Quote.to_collection_dict(Quote.query, page, per_page, 'api.get_quotes')
    return jsonify(data)
    # Get Quote

@bp.route("/quote", methods=['POST'])
@token_auth.login_required
def create_quote():
    data = json.loads(request.data) or {'error: no data'}
    if 'name' not in data \
        or 'email' not in data \
        or 'phone_number' not in data \
        or 'address' not in data \
        or 'est_volume' not in data \
        or 'description' not in data \
        or 'date' not in data \
        or 'time' not in data \
        or 'cost' not in data \
        or 'userid' not in data:
        print(data)
        return bad_request('must include username, email and password fields')

    quote = Quote()
    quote.from_dict(data)
    db.session.add(quote)
    db.session.commit()
    response = jsonify(lead.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_quotes', id=lead.lead_id)
    return response

@bp.route("/quote/<int:lead_id>", methods=['POST'])
@token_auth.login_required
def create_quote_from_lead():
    data = json.loads(request.data) or {'error: no data'}
    if 'name' not in data \
        or 'email' not in data \
        or 'phone_number' not in data \
        or 'address' not in data \
        or 'est_volume' not in data \
        or 'description' not in data \
        or 'date' not in data \
        or 'time' not in data \
        or 'cost' not in data \
        or 'lead_id' not in data \
        or 'userid' not in data:
        print(data)
        return bad_request('must include username, email and password fields')

    quote = Quote()
    quote.from_dict(data)
    db.session.add(quote)
    db.session.commit()
    response = jsonify(lead.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_quotes', id=lead.lead_id)
    return response

