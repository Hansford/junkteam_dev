import json
from flask import jsonify, request, url_for, g
from junkteam.models import Timesheet
from junkteam.api import bp
from junkteam.api.errors import bad_request
from junkteam import db
from junkteam.api.auth import token_auth
from flask_login import current_user
from datetime import datetime


@bp.route('/timesheets/<int:id>', methods=['GET'])
@token_auth.login_required
def get_timesheet(id):
    return jsonify(Timesheet.query.get_or_404(id).to_dict())

@bp.route('/timesheets', methods=['GET'])
@token_auth.login_required
def get_timesheets():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 1000, type=int), 100)
    data = Timesheet.to_collection_dict(Timesheet.query, page, per_page, 'api.get_timesheets')
    return jsonify(data)

@bp.route('/timesheet/clockin', methods=['POST'])
@token_auth.login_required
def create_timesheet(): 
    """API End-point for timesheet creation"""
    timesheet = Timesheet(userid=g.current_user.id, clock_in_timestamp=datetime.today())
    print(f"Timesheet to Commit: {timesheet}")
    db.session.add(timesheet)
    db.session.commit()
    print(f" Committed: {timesheet}")
    response = jsonify(timesheet.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_timesheets', id=timesheet.id)
    print("SUCCESS")
    return response

@bp.route('/timesheet/clockout', methods=['POST'])
@token_auth.login_required
def clockout(): 
    print(request.data)
    data = json.loads(request.data)
    timesheet_id = data['timesheet_id']
    print(f"Data: {timesheet_id}")
    timesheet = Timesheet.query.get(timesheet_id) 
    print(f"TimeSheet: {timesheet}")
    print("="*5 + f"Clock Out Dev" + "="*5 )
    print(timesheet.clock_out_timestamp)
    if timesheet.clock_out_timestamp == None:
        timesheet.clock_out_timestamp = datetime.today()
        db.session.commit()
        response = jsonify(timesheet.to_dict())
        response.status_code = 201
        print("SUCCESS")
        return response