"""API Routed for interacting with Leads"""
import json
from flask import jsonify, request, url_for
from junkteam.models import Lead
from junkteam.api import bp
from junkteam.api.errors import bad_request
from junkteam import db
from junkteam.api.auth import token_auth
from junkteam import LOAD_SIZES_CHOICES

# Seach Lead By ID 
@bp.route('/leads/<int:id>', methods=['GET'])
#token_auth.login_required
def get_lead(id):
    return jsonify(Lead.query.get_or_404(id).to_dict())

# Get Leads API
@bp.route('/leads', methods=['GET'])
#@token_auth.login_required
def get_leads():
    """API E-ndpoint to retreive all leads with pagination"""
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 1000, type=int), 1)
    data = Lead.to_collection_dict(Lead.query, page, per_page, 'api.get_leads')
    return jsonify(data)

@bp.route('/availability', methods=['POST'])
#@token_auth.login_required
def availability(): 
    print('checking availability...')
    data = json.loads(request.data)
    print(f"Data: {data}")
    leads = Lead.query.filter_by(date=data['date']).all()
    leads_dict = {}
    if leads:
        for id, lead in enumerate(leads):
            leads_dict[lead.lead_id] = str(lead.time)
            print(lead.lead_id, lead.time)
        print(f"Disable Time Dictionary: {leads_dict}")
        response = jsonify(leads_dict)
        print(f"Res: {response.data}")
        response.status_code = 201
        return response
    else:
        no_dates = {}
        no_dates['0'] = "0:00"
        response = jsonify(no_dates)
        print(f"Res: {response.data}")
        response.status_code = 201
        return response

@bp.route('/lead', methods=['POST'])
#@token_auth.login_required
def create_lead(): 
    """API End-point for lead creation"""
    print("Incoming Data")
    #data = request.form
    data = json.loads(request.data) or {'error: no data'}
    est_load_display = dict(LOAD_SIZES_CHOICES).get(data['est_volume'])
    if 'name' not in data \
        or 'email' not in data \
        or 'phone_number' not in data:
        print(data)
        return bad_request('must include username, email and password fields')

    lead = Lead()
    lead.from_dict(data)
    lead.est_volume = est_load_display
    lead.cost = float(data['est_volume'])
    print(f"Lead to Commit: {lead}")
    db.session.add(lead)
    db.session.commit()
    print(f" Committed: {lead}")
    response = jsonify(lead.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_leads', id=lead.lead_id)
    print("SUCCESS")
    return response
