from flask import jsonify, g
from junkteam import db
from junkteam.api import bp
from junkteam.api.auth import basic_auth
from junkteam.api.auth import token_auth


@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
def get_token():
    token = g.current_user.get_token()
    db.session.commit()
    print("token: " + token)
    return jsonify({'token': token})


@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    g.current_user.revoke_token()
    db.session.commit()
    return '', 204

