import json
from flask import jsonify, request, url_for
from junkteam.models import Lead
from junkteam.api import bp
from junkteam.api.errors import bad_request
from junkteam import db
from junkteam.api.auth import token_auth
from junkteam import LOAD_SIZES_CHOICES
from json import loads


@bp.route('/dashboard/calender_month_events_json', methods=['GET'])
#@limiter.limit("1000/hour")
#@token_auth.login_required
def calender_month_events_json():
    print("Calling calender_month_events JSON") 
    data = loads(request.data)
    month = data['month']
    #month = str(date)[4:-2]
    search = "%-{}-%".format(month)
    leads = Lead.query.filter(Lead.date.like(search)).all()
    leads_dict = {}
    if leads:
        for id, lead in enumerate(leads):
            leads_dict[str(id)] = lead.to_dict()
            print(lead, id)
        response = jsonify(leads_dict)
        response.status_code = 201
        return response
    else:
        response = jsonify(leads_dict)
        response.status_code = 204
        return response

@bp.route('/dashboard/calender_month_events', methods=['GET'])
#@limiter.limit("1000/hour")
#@token_auth.login_required
def calender_month_events():
    print("Calling calender_month_events") 
    start = str(request.args.get('start'))
    end = str(request.args.get('end'))
    start_date = start[:-15]
    end_date = end[:-15]
    print(f"Start Date: {start_date}")
    print(f"End Date: {end_date}")
    leads = Lead.query.filter(Lead.date.between(start_date, end_date)).all()
    leads_dict = {}
    if leads:
        lead_data_list = []
        for id, lead in enumerate(leads):
            leads_dict[str(id)] = lead.to_dict()
            print(lead, id)
            lead_data = {
                'title' : f"Name: {lead.name} \n addr: {lead.address} \n ph#: {lead.phone_number} ",
                'start': lead.date,
                'id': lead.lead_id
            }
            lead_data_list.append(lead_data)
        response = jsonify(lead_data_list)        
        print(f'Tring to send event {response}')
        return response
    else:
        print("FAILED TO GET MONTH'S LEADS")
        response = jsonify(leads_dict)
        response.status_code = 204
        return response