import json
from flask import jsonify, request, url_for
from junkteam.models import Order, Payment, Expense 
from junkteam.api import bp
from junkteam.api.errors import bad_request
from junkteam import db
from junkteam.api.auth import token_auth
from json import loads
from datetime import date


@bp.route('/report/main_report', methods=['GET'])
#@token_auth.login_required
def main_report():
    
    today = date.today()
    search = "%{}%".format(today)
    payments = Payment.query.filter(Payment.timestamp.like(search)).all()
    expenses = Expense.query.filter(Expense.timestamp.like(search)).all()
    print(f"Today's payments: {payments}")
    print(f"Today's expenses: {expenses}")
    days_revenue = 0
    for payment in payments:
        days_revenue += payment.amount
    days_expenses = 0
    days_fuel = 0
    days_dump = 0 
    for expense in expenses:
        days_expenses += expense.cost 
        if expense.expense_type == "Dump":
            days_dump += expense.cost
        if expense.expense_type == "Fuel":
            days_fuel += expense.cost
    print(f"Total Days Expense: {days_expenses}")


    data = {
        'target': {
            'revenue': '1500',
            'fuel': '200',
            'dump': '200'},
        'actual': {
            'revenue': days_revenue,
            'fuel': days_fuel,
            'dump': days_dump},
    }
        
    response = jsonify(data)
    return response