from datetime import datetime, timedelta
from hashlib import md5
from flask import jsonify, url_for
from time import time
from flask_login import UserMixin
#from flask_marshmallow import Marshmallow, pprint
from passlib.hash import sha256_crypt
import jwt
from junkteam import db, login, app
import base64
import os

#ma = Marshmallow(app)
@login.user_loader
def load_user(id):
    return User.query.get(int(id))

# Followers (example if required later, see User model)
# followers = db.Table('followers', db.Column('follower_id', db.Integer, db.ForeignKey('user.id')), db.Column('followed_id', db.Integer, db.ForeignKey('user.id')))

class PaginatedAPIMixin(object):
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {'items': [item.to_dict() for item in resources.items],
                '_meta': {
            'page': page,
            'per_page': per_page,
            'total_pages': resources.pages,
            'total_items': resources.total
        },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page,
                                **kwargs),
            'next': url_for(endpoint, page=page + 1, per_page=per_page,
                            **kwargs) if resources.has_next else None,
            'prev': url_for(endpoint, page=page - 1, per_page=per_page,
                            **kwargs) if resources.has_prev else None
        }
        }
        return data

# User Model
class User(PaginatedAPIMixin, UserMixin, db.Model):
    '''
    User Model
    '''
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    leads = db.relationship('Lead', backref='client_leads', lazy='dynamic')
    quotes = db.relationship('Quote', backref='client_quotes', lazy='dynamic')
    orders = db.relationship('Order', backref='client_orders', lazy='dynamic')
    payments = db.relationship('Payment', backref='client_payments', lazy='dynamic')
    timesheets = db.relationship('Timesheet', backref='user_timesheets', lazy='dynamic')
    # expand for required client info (business contact, tax code, etc)
    info = db.Column(db.String(256))
    last_seen = db.Column(db.DateTime, default=datetime.now)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    roles = db.relationship('Role', secondary='user_roles')
    

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'leads': self.leads.count(),
            'quotes': self.quotes.count(),
            'orders': self.orders.count()
        }

        if include_email:
            data['email'] = self.email
        return data

    def from_dict(self, data, new_user=False):

        for field in ['username', 'email', 'info']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])
   # example of many to many if required later
   # followed = db.relationship('User', secondary=followers,primaryjoin=(followers.c.follower_id == id),secondaryjoin=(followers.c.followed_id == id),backref=db.backref('followers', lazy='dynamic'), lazy='dynamic')

    # so they can message admin from app
    messages_sent = db.relationship(
        'Message', foreign_keys='Message.sender_id', backref='author', lazy='dynamic')
    messages_received = db.relationship(
        'Message', foreign_keys='Message.recipient_id', backref='recipient', lazy='dynamic')
    last_message_read_time = db.Column(db.DateTime)

    def __repr__(self):
        return f'Username: {self.username}'

    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(Message.timestamp > last_read_time).count()

    def set_password(self, password):
        self.password_hash = sha256_crypt.encrypt(password)

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password_hash)

   # def avatar(self, size):
   #     digest = md5(self.email.lower().encode('utf-8')).hexdigest()
   #     return 'https://www.gravatar.com/avatar/{}?d=mm&s={}'.format(digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode({'reset_password': self.id, 'exp': time() + expires_in}, app.secret_key, algorithm='HS256').decode('utf-8')

   # def follow(self, user):
   #    if not self.is_following(user):
   #         self.followed.append(user)

   # def unfollow(self, user):
   #     if self.is_following(user):
   #         self.followed.remove(user)

   # def is_following(self, user):
   #     return self.followed.filter(followers.c.followed_id == user.id).count() > 0

   # def followed_posts(self):
   #     followed = Post.query.join(followers, (followers.c.followed_id == Post.userid)).filter(followers.c.follower_id == self.id)
   #     own = Post.query.filter_by(userid=self.id)
   #     return followed.union(own).order_by(Post.timestamp.desc())

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        print(f"Trying to revoke Token")
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)
        db.session.add(self)
        return self.token_expiration

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.now():
            return None
        return user

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.secret_key, algorithms=[
                            'HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def get_roles(self):
            return str(self.roles)

# Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)
    def __repr__(self):
        return (self.name)

# Define the UserRoles association table
class UserRoles(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles.id', ondelete='CASCADE'))

# Commercial account quotes/orders schema
class Lead(PaginatedAPIMixin, db.Model):
    lead_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=False)
    email = db.Column(db.String(120), unique=False)
    phone_number = db.Column(db.String(64), nullable=False, unique=False)
    address = db.Column(db.String(64), unique=False)
    est_volume = db.Column(db.String(64), unique=False)
    description = db.Column(db.String(64), unique=False)
    date = db.Column(db.String(25))
    time = db.Column(db.String(25))
    cost = db.Column(db.String(25))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    quoted = db.Column(db.Boolean, unique=False, default=False)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    quotes = db.relationship('Quote', backref='client', lazy=True)

    def __repr__(self):
        return 'ID:{}  Name:{} Email:{}'.format(self.lead_id, self.name, self.email)

    def to_dict(self, include_email=True):
        data = {
            'lead_id': self.lead_id,
            'name': self.name,
            'email': self.email,
            'phone_number': self.phone_number,
            'address': self.address,
            'est_volume': self.est_volume,
            'description': self.description,
            'date': self.date,
            'time': self.time,
            'cost': self.cost,
            'timestamp': self.timestamp,
            'userid': self.userid
        }
        return data

    def from_dict(self, data):
        for field in ["name", "email", "address", "cost", "date", \
                    "description", "est_volume", "phone_number", \
                    "time", "userid"]:
            if field in data:
                setattr(self, field, data[field])
        
class Quote(PaginatedAPIMixin, db.Model):
    __tablename__ = 'quote'
    quote_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=False)
    email = db.Column(db.String(120), unique=False)
    phone_number = db.Column(db.String(64), nullable=False, unique=False)
    address = db.Column(db.String(64), unique=False)
    est_volume = db.Column(db.String(64), unique=False)
    charge = db.relationship("Charge", uselist=False, back_populates="quote")
    description = db.Column(db.String(64), unique=False)
    date = db.Column(db.String(25))
    time = db.Column(db.String(25))
    cost = db.Column(db.String(25))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    deleted = db.Column(db.Boolean(), default=False)
    viewed = db.Column(db.Boolean, unique=False, default=False)
    ordered = db.Column(db.Boolean(), default=False)
    lead_id = db.Column(db.Integer,
                        db.ForeignKey('lead.lead_id'), nullable=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    orders = db.relationship('Order', backref='client', lazy='dynamic')

    def __repr__(self):
        return 'ID:{}  Name:{}'.format(self.lead_id, self.name)

    def to_dict(self, include_email=False):
        data = {
            'lead_id': self.lead_id,
            'name': self.name,
            'email': self.email,
            'phone_number': self.phone_number,
            'address': self.address,
            'est_volume': self.est_volume,
            'description': self.description,
            'date': self.date,
            'time': self.time,
            'cost': self.cost,
            'timestamp': self.timestamp,
            'userid': self.userid
        }
        return data

class Order(PaginatedAPIMixin, db.Model):
    order_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=False)
    email = db.Column(db.String(120), unique=False)
    phone_number = db.Column(db.String(64), nullable=False, unique=False)
    address = db.Column(db.String(64), unique=False)
    description = db.Column(db.String(64), unique=False)
    date = db.Column(db.String(25))
    time = db.Column(db.String(25))
    sub_total = db.Column(db.Float())
    tax = db.Column(db.Float())
    total = db.Column(db.Float())
    amount_due = db.Column(db.Float())
    deleted = db.Column(db.Boolean(), default=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    ##TODO: Should quote_id be nullable? 
    quote_id = db.Column(db.Integer, db.ForeignKey('quote.quote_id'), nullable=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    payment_id = db.relationship('Payment', backref='client', lazy='dynamic')
    paid = db.Column(db.Boolean, unique=False, default=False)

    def __repr__(self):
        return 'ID:{}  Name:{}'.format(self.lead_id, self.name)

    def to_dict(self, include_email=False):
        data = {

            'lead_id': self.lead_id,
            'name': self.name,
            'email': self.email,
            'phone_number': self.phone_number,
            'address': self.address,
            'est_volume': self.est_volume,
            'description': self.description,
            'date': self.date,
            'time': self.time,
            'cost': self.cost,
            'timestamp': self.timestamp,
            'userid': self.userid
        }
        return data

'''
class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ref_num = db.Column(db.Integer, primary_key=True)
    cost = db.Column(db.String(250))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    def __repr__(self):
        return '<Post {}>'.format(self.body)
'''
class Payment(PaginatedAPIMixin, db.Model):
    payment_id = db.Column(db.Integer, primary_key=True)
    method = db.Column(db.String(64), nullable=False, unique=False)
    amount = db.Column(db.Float, nullable=False, unique=False)
    receipt_id = db.Column(db.String(64), nullable=True, unique=False)
    sent = db.Column(db.Boolean, default=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    order_id = db.Column(db.Integer, db.ForeignKey('order.order_id'), nullable=False)
    def __repr__(self):
        return 'Payment ID:{}  Method:{} Amount:{}'.format(self.payment_id, self.method, self.amount)

    def to_dict(self, include_email=False):
        data = {

            'payment_id': self.payment_id,
            'method': self.method,
            'amount': self.amount,
            'receipt_id': self.receipt_id,
            'timestamp': self.timestamp
           
        }
        return data

class Expense(PaginatedAPIMixin, db.Model):
    expense_id = db.Column(db.Integer, primary_key=True)
    expense_type = db.Column(db.String(64), nullable=False, unique=False)
    invoice_id = db.Column(db.String(120), unique=False)
    cost = db.Column(db.Float())
    mileage = db.Column(db.String(25))
    weight = db.Column(db.String(64), unique=False)
    description = db.Column(db.String(120), unique=False)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    

    def __repr__(self):
        return f'ID:{self.expense_id} Type:{self.expense_type} Cost:{self.cost}'

    def to_dict(self, include_email=False):
        data = {
            'expense_id': self.expense_id,
            'expense_type': self.expense_type,
            'invoice_id': self.invoice_id,
            'cost': self.cost,
            'mileage': self.mileage,
            'weight': self.weight,
            'description': self.description,
            'timestamp': self.timestamp,
            'userid': self.userid
        }
        return data

class Items(db.Model):
    __tablename__ = 'items'
    item_id = db.Column(db.Integer, db.ForeignKey('item.id'), primary_key=True)
    charge_id = db.Column(db.Integer, db.ForeignKey('charge.id'), primary_key=True)

    quantity = db.Column(db.Integer(), nullable=False, default=1)

    item = db.relationship('Item', backref='item_items')
    charge = db.relationship('Charge', backref='charge_items')

class Charge(db.Model):
    __tablename__ = 'charge'  
    id = db.Column(db.Integer, primary_key=True)
    #items = db.relationship('Item', secondary=items, lazy='subquery',
    #    backref=db.backref('charge', lazy=True)) 
    quote_id = db.Column(db.Integer, db.ForeignKey('quote.quote_id'))
    quote = db.relationship("Quote", back_populates="charge") 

    def __repr__(self):
        """Return a representation of a ItemCart instance."""
        return f"<Charge ID#: {self.id}>"

    def get_subtotal(self):
        print(f"ID: {self.id}")
        all_items = Items.query.filter_by(charge_id=self.id).all()
        sub_total = 0.0
        for item in all_items:
            sub_total += item.item.cost * item.quantity
        return sub_total
    
    def get_tax(self):
        tax = self.get_subtotal() * 0.13
        return tax
    
    def get_total(self):
        total = self.get_subtotal() * 1.13
        return total

class Item(db.Model):
    __tablename__ = 'item'
    id =  db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description = db.Column(db.String(256))
    cost = db.Column(db.Float())
    lookup = db.Column(db.String(32))

# Messages schema
class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sender_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    recipient_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.now)

    def __repr__(self):
        return '<Message {}>'.format(self.body)


class Timesheet(PaginatedAPIMixin, db.Model):
    __tablename__ = 'timesheet'
    id = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    clock_in_timestamp = db.Column(db.DateTime, nullable=False)
    clock_out_timestamp = db.Column(db.DateTime, nullable=True)
    
    def __repr__(self):
        return f'TimeSheet ID:{self.id} User ID: {self.userid}  Clock-In:{self.clock_in_timestamp  } Clock Out: {self.clock_out_timestamp}'

    def to_dict(self):
        data = {
            'id': self.id,
            'clock_in' : self.clock_in_timestamp,
            'clock_out': self.clock_out_timestamp,
            'userid': self.userid
        }
        return data

    def from_dict(self, data):
        for field in ["userid", "clock_in_timestamp", "clock_out_timestamp"]:
            if field in data:
                setattr(self, field, data[field])