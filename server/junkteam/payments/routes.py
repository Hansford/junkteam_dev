from flask import render_template, url_for, request, redirect, session
from junkteam.models import Order, Payment
from junkteam.payments import bp
from junkteam import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required
from junkteam.payments.forms import PaymentForm
from flask_login import current_user
from junkteam.payments.email import send_receipt_email

''' INTERNAL SUBDOMAIN BLUEPRINT '''



#Payments Dashboard - Loads in the current Payments 
@bp.route('/', methods=['GET', 'POST'])
@login_required
def payments():
    page = request.args.get('page', 1, type=int)

    payments = Payment.query.order_by(Payment.payment_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('payments.payments', page=payments.next_num) \
        if payments.has_next else None
    prev_url = url_for('payments.payments', page=payments.prev_num) \
        if payments.has_prev else None
    return render_template('payments/payments.html', title='Payments',
                           payments=payments.items,
                           next_url=next_url, prev_url=prev_url)


# 
@bp.route('/create_payment/<id>', methods=['GET', 'POST'])
@login_required
def create_payment(id):
    order = Order.query.get(id)
    form = PaymentForm()
    if form.validate_on_submit():
        
        payment = Payment(method=form.method.data, amount=form.amount.data,
                      receipt_id=form.receipt_id.data, order_id=order.order_id, userid=session["user_id"])
  
        db.session.add(payment)
        db.session.commit()
        print(f"Order AD: {order.amount_due}, Payment Am: {payment.amount}")
        print(f"T Order AD: {type(order.amount_due)}, T Payment Am: type({payment.amount})")

        order.amount_due = order.amount_due - payment.amount
        db.session.commit()

        if round(order.amount_due) == 0 or round(order.amount_due) == 0.0 :
            order.paid = True
            db.session.commit()
        

        # update order object amount_due, amount_paid. When amount_due = 0, mark as paid

        return redirect(url_for('payments.payments'))

    return render_template('payments/create_payment.html', form=form, order=order)

# Send Email Receipt
@bp.route('/send_receipt/<payment_id>', methods=['GET', 'POST'])
@login_required
def send_receipt(payment_id):
    print("Trying to send email...")
    payment = Payment.query.filter_by(payment_id=payment_id).first()
    if payment:
        print(f"Payment ID: {payment_id}")
        try: 
            order = Order.query.get(payment.order_id)
            sender_email = current_user.email
            print(f"Sender Email: {sender_email}")
            
            send_receipt_email(sender_email, payment, order)
            print("Email Receipt Send")
            payment.sent = True
            db.session.commit()
            return redirect(url_for('payments.payments'))
        
        except:
            print("Email failed")
            return redirect(url_for('payments.payments'))
        
    else:
        print("Payment query failed")
    return redirect(url_for('payments.payments'))
'''
# edit quotes
@bp.route('/edit_quote/<id>', methods=['GET', 'POST'])
@login_required
def edit_quote(id):
    quote = Quote.query.get(id)
    form = QuoteForm()
    if form.validate_on_submit():
        quote.name=form.name.data
        quote.email=form.email.data
        quote.phone_number=form.phone_number.data
        quote.address=form.address.data
        quote.est_volume=form.est_volume.data
        quote.description=form.description.data
        quote.date=form.dt.data
        quote.quote_id=id
        quote.userid=session["user_id"]
        quote.time=form.time.data
        quote.cost=form.cost.data
        db.session.commit()
        return redirect(url_for('quotes.quotes'))
    return render_template('quotes/edit_quote.html', form=form, quote=quote)




# Delete Quote
@bp.route('/delete_quote/<id>')
@login_required
def delete_quote(id):
    Quote.query.filter_by(quote_id=id).delete()
    db.session.commit()
    return redirect(url_for('quotes.quotes'))
'''