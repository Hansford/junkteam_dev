from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, StringField, DecimalField, SelectField, validators, ValidationError
from flask_wtf import Form


''' INTERNAL SUBDOMAIN BLUEPRINT '''

class PaymentForm(FlaskForm):
    method = SelectField('Method:', choices=[('Cash', 'Cash'), ('Credit', 'Credit'), ('Debit', 'Debit')])
    receipt_id = StringField('Receipt #:', [validators.DataRequired(), validators.Length(min=0, max=16)])
    amount = DecimalField('Payment:')
    submit = SubmitField('Submit Payment')


'''
    def validate_phone_number(form, field):
        if len(field.data) > 16:
            raise ValidationError('Invalid phone number.')
        try:
            input_number = phonenumbers.parse(field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
        except:
            input_number = phonenumbers.parse("+1"+field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
'''