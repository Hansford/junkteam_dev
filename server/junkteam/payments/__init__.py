from flask import Blueprint

''' INTERNAL SUBDOMAIN BLUEPRINT '''

bp = Blueprint('payments', __name__)
from junkteam.payments import routes
