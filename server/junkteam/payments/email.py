from flask import render_template
from junkteam import app
from junkteam.email import send_email

def send_receipt_email(sender_email, payment, order):
    print(f"Data for Email: {sender_email},  {payment}, {order.email}")
    send_email('Receipt - Junk Team ', sender=sender_email, recipients=order.email.split(), text_body=render_template(
        'email/receipt.txt', payment=payment, order=order), html_body=render_template('email/receipt.html', payment=payment, order=order))
