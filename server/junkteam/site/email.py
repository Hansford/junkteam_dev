from flask import render_template
from junkteam import app
from junkteam.email import send_email


def send_lead_notification_email(data):
    send_email('Lead Notification - Junk Team ', sender=app.config['ADMINS'], recipients=app.config['ADMINS'].split(), text_body=render_template(
        'email/lead_notification.txt'), html_body=render_template('email/lead_notification.html'))
