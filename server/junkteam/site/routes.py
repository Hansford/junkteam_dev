from flask import render_template, flash, redirect, url_for, request, jsonify
from junkteam import db, app #, limiter
from junkteam.site import bp
from junkteam.models import Lead, Timesheet
from junkteam.site.forms import LeadForm, LOAD_SIZES_CHOICES
from datetime import datetime
from flask_login import current_user, login_required
from json import dumps, loads
from junkteam.site.email import send_lead_notification_email
from datetime import date


@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()

@bp.route('/', methods=['GET', 'POST'])
@login_required
#@limiter.limit("1000/hour")
def dashboard():
    month = str(date.today())[4:-2]
    leads = Lead.query.filter(Lead.date.like(f"%{month}%")).all()
    print(f"This months leads: {leads}")
    today = str(date.today())
    timesheet = current_user.timesheets.filter(Timesheet.clock_in_timestamp.contains(today)).first()
    print(f"timesheet: {timesheet}")
    return render_template('dashboard.html', leads = leads, timesheet=timesheet)
    

@bp.route('/confirmation/<lead_id>', methods=['GET', 'POST'])
#@limiter.limit("200 per hour")
def confirmation(lead_id):

    lead = Lead.query.filter_by(lead_id=lead_id).first()
    send_lead_notification_email(lead)
    return render_template('confirmation.html', lead=lead)


@bp.route('/benefits')
def benefits():

    return redirect(url_for('site.index', _anchor='services'))












''' For jQuery handling


@bp.route('/_process', methods=['POST'])
def _process():
    form = QuoteForm()
    data = jsonify(form.data)
    if form.validate_on_submit():
        return data
    else:
        return render_template("index.html", form=form)
'''
