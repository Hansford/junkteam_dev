from flask import render_template, url_for, request, redirect, session, flash
from junkteam.orders import bp
from junkteam.models import Quote, Order, Items
from junkteam import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required
from junkteam.orders.forms import OrderForm

''' INTERNAL SUBDOMAIN BLUEPRINT '''

@bp.route('/', methods=['GET', 'POST'])
@login_required
def orders():
    
    page = request.args.get('page', 1, type=int)
    orders = Order.query.order_by(Order.order_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('orders.orders', page=orders.next_num) \
        if orders.has_next else None
    prev_url = url_for('orders.orders', page=orders.prev_num) \
        if orders.has_prev else None
    return render_template('orders/orders.html', title='Orders',
                           orders=orders.items,
                           next_url=next_url, prev_url=prev_url)




#Order Summary  - Loads in the selected Order 
@bp.route('/order/<order_id>', methods=['GET', 'POST'])
@login_required
def order(order_id):

    order = Order.query.get(order_id)
    if order:                                       # check if order exists
        quote = Quote.query.get(order.quote_id)
        items = Items.query.filter_by(charge_id=quote.charge.id).all()
        return render_template('orders/order.html', order=order, items=items)
    id = int(order_id)
    if id != 0:
        order = Order.query.get(id-1)
        flash("Last order")
        return redirect(url_for('orders.order', order_id=order.order_id))
    order = Order.query.get(id+1)
    return redirect(url_for('orders.order', order_id=order.order_id))

# make Create order from quote
@bp.route('/create_order/<id>', methods=['GET', 'POST'])
@login_required
def order_from_quote(id):
    quote = Quote.query.get(id)
    order = create_order(quote)
    return redirect(url_for('orders.order', order_id=order.order_id))


def create_order(quote):
    order = Order(name=quote.name, email=quote.email,
                      phone_number=quote.phone_number,
                      address=quote.address,
                      description=quote.description,
                      quote_id=quote.quote_id, userid=session["user_id"],
                      date=quote.date, time=quote.time)
    sub_total = quote.charge.get_subtotal() 
    order.sub_total = sub_total
    order.tax = sub_total * 0.13
    order.total = sub_total * 1.13   
    order.amount_due = order.total             
    try:
        db.session.add(order)
        db.session.commit()
    except:
        return render_template('errors/404.html'), 404
    quote.ordered = True
    db.session.commit()
    return order


# Disabled -  Create order route
'''
@bp.route('/create_order', methods=['GET', 'POST'])
@login_required
def create_order():
    form = QuoteForm()
    if form.validate_on_submit():
        quote = Quote(name=form.name.data, email=form.email.data,
                      phone_number=form.phone_number.data,
                      address=form.address.data,
                      est_volume=form.est_volume.data,
                      description=form.description.data, date=form.dt.data,
                      time=form.time.data, cost=form.cost.data)
        db.session.add(quote)
        db.session.commit()
        return redirect(url_for('admin.quotes'))
    return render_template('admin/quote.html', form=form)
'''

# make Delete order - add rols requirements

@bp.route('/delete_order/<id>')
@login_required
def delete_order(id):
    Order.query.filter_by(order_id=id).delete()
    db.session.commit()
    return redirect(url_for('orders.orders'))

@bp.route('/delete_order/<id>')
@login_required
def delete_quote(id):
    order = Order.query.get_or_404(id)
    if order.deleted:
        abort(404)
    order.deleted = True
    db.session.commit()
    return redirect(url_for('orders.orders'))

