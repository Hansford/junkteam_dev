from flask import Blueprint

''' INTERNAL SUBDOMAIN BLUEPRINT '''

bp = Blueprint('orders', __name__)
from junkteam.orders import routes
