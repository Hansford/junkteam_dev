from flask import session, request, jsonify
from flask_socketio import emit, join_room, leave_room
from junkteam.models import User
from .. import socketio
import json, time

@socketio.on('connect', namespace='/technicians')
def server_event():   
    print('Server Event!!!!!') 
    emit('server_event', {'data': 'This is emitting from the server'}, namespace='/technicians')   

@socketio.on('button', namespace='/technicians')
def button(button_ack):
    print ('Client pressed a button') 
    emit('button_ack', {'data': 'Button function called'}, namespace='/technicians')

@socketio.on('get_users', namespace='/technicians')
def get_users(message):
    users = User.query.all()
    data = [User.to_dict(user) for user in users]
    mydict={}
    for item in data:
        mydict[item['id']] = item['username']
    var = list(mydict.values())
    print('data: ' + str(var))
    emit('message', {'msg': var})

    
   

