from flask import render_template, url_for, request, redirect
from junkteam.technicians import bp
from junkteam.models import Lead, Quote, User, UserRoles, Role
from junkteam import app, db, socketio
# from werkzeug.urls import url_parse
from flask_login import login_required
import flask
from time import sleep



@bp.route('/', methods=['GET'])
@login_required
def technicians():
    page = request.args.get('page', 1, type=int)
    user_roles = User.query.join(UserRoles).join(Role).filter((UserRoles.user_id == User.id) & (UserRoles.role_id == 3)).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('technicians.technicians', page=user_roles.next_num) \
        if user_roles.has_next else None
    prev_url = url_for('technicians.technicians', page=user_roles.prev_num) \
        if user_roles.has_prev else None
    return render_template('technicians/technicians.html', title='Technicians',
                           user_roles=user_roles.items,
                           next_url=next_url, prev_url=prev_url)
'''
    user_list = User.query.order_by(User.id.desc())
    print(type(user_list))
    techs = []
    for user in user_list:
        if user.get_roles() == "[TECH]":
            techs.append(user)
    for tech in techs:
        print(f"Tech Usernames: {tech.username}")
    
    return render_template('technicians/technicians.html', title='Techs',
                           techs=techs)
'''
    