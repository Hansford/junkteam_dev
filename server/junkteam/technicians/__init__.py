from flask import Blueprint


bp = Blueprint('technicians', __name__)
from junkteam.technicians import routes, events
