
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user, login_manager
from flask_migrate import Migrate
from flask_mail import Mail
#from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_cors import CORS
from functools import wraps
from flask_socketio import SocketIO
import os

import eventlet
eventlet.monkey_patch()


basedir = os.path.abspath(os.path.dirname(__file__))
#app = Flask(__name__, subdomain_matching=True)
app = Flask(__name__)
socketio = SocketIO()
socketio.init_app(app, message_queue='redis://', async_mode = 'eventlet')

#make config loader or use .env with ?os.getenviron?
#app.config['SERVER_NAME'] = 'junkteam.ca'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
app.secret_key = '45j3b45khjb34ajfbw38rgfuq30ugfqw3igfqno3ugfsfsvbvavFE34h5b3hj4x'
app.config['POSTS_PER_PAGE'] = 10
app.config['MAIL_SERVER'] = 'smtp.zoho.com'
app.config['MAIL_PORT'] = '587'
app.config['MAIL_USE_TLS'] = '1'
app.config['MAIL_USERNAME'] = 'admin@junkteam.ca'
app.config['MAIL_PASSWORD'] = 'Tx7j1388'
app.config['ADMINS'] = 'admin@junkteam.ca'


LOAD_SIZES_CHOICES = [('75.00', 'Min'),('120.00', '1/8'), ('160.00', '1/6'), ('210.00', '1/4'), ('315.00', '1/2'), ('400.00', '3/4'), ('465.00', '1')]  
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
db = SQLAlchemy(app)

migrate = Migrate(app, db)

mail = Mail(app)
'''
limiter = Limiter(
    app,
    key_func=get_remote_address,
    default_limits=["5000 per day", "2000 per hour"],
)
'''


login = LoginManager(app)
login.login_view = 'auth.login'
login.login_message_category = "warning"


def role_required(role="ANY"):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
               return login.unauthorized()
            urole = current_user.get_roles()
            if ( ( role not in urole) and (role != "ANY")):
                print("Error!!!")
                return login.unauthorized()      
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper


from junkteam.site import bp as site_bp
from junkteam.api import bp as api_bp
from junkteam.admin import bp as admin_bp
from junkteam.leads import bp as leads_bp
from junkteam.quotes import bp as quotes_bp
from junkteam.orders import bp as orders_bp
from junkteam.payments import bp as payments_bp
from junkteam.expenses import bp as expenses_bp
from junkteam.auth import bp as auth_bp
from junkteam.technicians import bp as technicians_bp
from junkteam.reports import bp as reports_bp
from junkteam.errors import bp as errors_bp
from junkteam.chat import chat as chat_bp


app.register_blueprint(site_bp)
app.register_blueprint(api_bp, url_prefix='/api')
app.register_blueprint(auth_bp, url_prefix='/auth')
app.register_blueprint(admin_bp, url_prefix='/admin')
app.register_blueprint(leads_bp, url_prefix='/leads')
app.register_blueprint(quotes_bp, url_prefix='/quotes')
app.register_blueprint(orders_bp, url_prefix='/orders')
app.register_blueprint(payments_bp, url_prefix='/payments')
app.register_blueprint(expenses_bp, url_prefix='/expenses')
app.register_blueprint(technicians_bp, url_prefix='/technicians')
app.register_blueprint(reports_bp, url_prefix='/reports')
app.register_blueprint(errors_bp, url_prefix='/error')
app.register_blueprint(chat_bp, url_prefix='/chat')
