from flask_wtf import FlaskForm
from wtforms import SubmitField, DecimalField, TextAreaField, StringField, SelectField, validators, ValidationError
from flask_wtf import Form


''' INTERNAL SUBDOMAIN BLUEPRINT '''

class ExpenseForm(FlaskForm):
    expense_type = SelectField('Expense Type:', choices=[(
        'Dump', 'Dump'), ('Fuel', 'Fuel'), ('Misc', 'Misc')])
    invoice_id = StringField('Invoice ID:', [validators.DataRequired()])
    cost = DecimalField('Cost:', [validators.DataRequired()])
    mileage = StringField('Mileage:')
    weight = StringField('Weight:')
    description = TextAreaField('Description/Notes:', [validators.Length(min=0, max=1000)])
    submit = SubmitField()

'''
    def validate_phone_number(form, field):
        if len(field.data) > 16:
            raise ValidationError('Invalid phone number.')
        try:
            input_number = phonenumbers.parse(field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
        except:
            input_number = phonenumbers.parse("+1"+field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
'''