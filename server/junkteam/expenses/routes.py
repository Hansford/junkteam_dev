from flask import render_template, url_for, request, redirect, session
from junkteam.expenses import bp
from junkteam import app, db
from junkteam.models import Expense
from junkteam.expenses.forms import ExpenseForm
# from werkzeug.urls import url_parse
from flask_login import login_required



#expenses Dashboard - Loads in the expenses
@bp.route('/', methods=['GET', 'POST'] )
@login_required
def expenses():
    page = request.args.get('page', 1, type=int)
    expenses = Expense.query.order_by(Expense.expense_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('expenses.expenses', page=expenses.next_num) \
        if expenses.has_next else None
    prev_url = url_for('expenses.expenses', page=expenses.prev_num) \
        if expenses.has_prev else None
    return render_template('expenses/expenses.html', title='Expenses',
                           expenses=expenses.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/create_expense', methods=['GET', 'POST'])
@login_required
def create_expense():
    form = ExpenseForm()
    if form.validate_on_submit():
        expense = Expense(expense_type=form.expense_type.data, invoice_id=form.invoice_id.data, cost=form.cost.data,
                    mileage=form.mileage.data, weight=form.weight.data, description=form.description.data, userid=session["user_id"])
        db.session.add(expense)
        db.session.commit()
        return redirect(url_for('expenses.expenses'))
    return render_template("expenses/create_expense.html", form=form)



''' Don't edit or delete expenses - add admin priveledge
@bp.route('/edit_lead/<id>', methods=['GET', 'POST'])
@login_required
def edit_lead(id):
    lead = Lead.query.get(id)
    form = LeadForm()
    if form.validate_on_submit():
        lead.name=form.name.data
        lead.email=form.email.data
        lead.phone_number=form.phone_number.data
        lead.address=form.address.data
        lead.est_volume=form.est_volume.data
        lead.description=form.description.data
        lead.date=form.dt.data
        lead.lead_id=id
        lead.userid=session["user_id"]
        lead.time=form.time.data
        lead.cost=form.cost.data
        db.session.commit()
        return redirect(url_for('leads.leads'))
    return render_template('leads/update_lead.html', form=form, lead=lead)

# Delete Lead
@bp.route('/delete_lead/<id>')
@login_required
def delete_lead(id):
    Lead.query.filter_by(lead_id=id).delete()
    db.session.commit()
    return redirect(url_for('leads.leads'))

'''