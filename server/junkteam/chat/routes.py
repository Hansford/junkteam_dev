from flask import session, redirect, url_for, render_template, request
from . import chat
from .forms import LoginForm
from flask_login import current_user


''' INTERNAL CHAT ROUTES '''

@chat.route('/', methods=['GET', 'POST'])
def index():
    """Login form to enter a room."""
    form = LoginForm()
    if form.validate_on_submit():
        session['name'] = current_user.username
        session['room'] = form.room.data
        return redirect(url_for('chat.tech_chat'))
    elif request.method == 'GET':
        form.room.data = session.get('room', '')
    return render_template('chat/index.html', form=form)


@chat.route('/chat')
def tech_chat():
    """Chat room. The user's name and room must be stored in
    the session."""
    name = session.get('name', '')
    room = session.get('room', '')
    if name == '' or room == '':
        return redirect(url_for('chat.index'))
    return render_template('chat/chat.html', name=name, room=room)

''' CLIENT CHAT '''

@chat.route('/chat')
def chat():
    """Chat room. The user's name and room must be stored in
    the session."""
    if current_user.is_authenticated:
        session['name'] = current_user.username
        session['room'] =  'chat'
    elif current_user.is_anonymous:
        session['name'] = 'Client'
        session['room'] =  'chat'
    name = session.get('name', '')
    room = session.get('room', '')
    if name == '' or room == '':
        return redirect(url_for('chat.index'))
    return render_template('chat/chat.html', name=name, room=room)  