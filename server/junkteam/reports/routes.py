from flask import render_template # url_for, request, redirect, session, flash, abort
# from junkteam.models import Lead, Quote, Charge, Item, Items, Order, Expense, Payment
from junkteam.reports import bp
#from junkteam import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required

''' INTERNAL SUBDOMAIN BLUEPRINT '''

# Login page


#Reports Dashboard -
@bp.route('/', methods=['GET', 'POST'])
@login_required
def reports():
    return render_template('reports/reports.html', title="Reports")