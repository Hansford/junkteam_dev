from flask import Blueprint

''' INTERNAL SUBDOMAIN BLUEPRINT '''

bp = Blueprint('reports', __name__)

from junkteam.reports import routes
