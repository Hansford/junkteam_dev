from flask import render_template, url_for, request, redirect
from junkteam.admin import bp
from junkteam import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required
from junkteam.models import Lead, Quote
from junkteam.quotes.forms import QuoteForm



#Leads Dashboard - Loads in the leads
@bp.route('/', methods=['GET', 'POST'])
@login_required
def admin():
    page = request.args.get('page', 1, type=int)
    leads = Lead.query.order_by(Lead.lead_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('leads.leads', page=leads.next_num) \
        if leads.has_next else None
    prev_url = url_for('leads.leads', page=leads.prev_num) \
        if leads.has_prev else None
    return render_template('admin/junkadmin.html', title='JunkTeam Admin',
                           leads=leads.items,
                           next_url=next_url, prev_url=prev_url)


# Create qute from Lead id
@bp.route('/create_quote/<id>', methods=['GET', 'POST'])
@login_required
def quote_from_lead(id):
    lead = Lead.query.get(id)
    form = QuoteForm()
    if form.validate_on_submit():
        quote = Quote(name=form.name.data, email=form.email.data,
                      phone_number=form.phone_number.data,
                      address=form.address.data,
                      est_volume=form.est_volume.data,
                      description=form.description.data, date=form.dt.data,
                      lead_id=id, userid=session["user_id"], time=form.time.data, 
                      cost=form.cost.data)


        db.session.add(quote)
        db.session.commit()
        return redirect(url_for('admin.admin'))
    return render_template('admin/junkadmin.html', form=form, lead=lead)
