from flask import Blueprint

''' INTERNAL SUBDOMAIN BLUEPRINT '''

bp = Blueprint('leads', __name__)
from junkteam.leads import routes
