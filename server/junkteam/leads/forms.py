from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, StringField, SelectField, validators, ValidationError
import phonenumbers
from flask_wtf import Form
from wtforms.fields.html5 import DateField
from junkteam import LOAD_SIZES_CHOICES
''' INTERNAL SUBDOMAIN BLUEPRINT '''

class LeadForm(FlaskForm):
    name = StringField('Name:', [validators.DataRequired(), validators.Length(min=0, max=256)])
    email = StringField('Email:', [validators.DataRequired(), validators.Email()])
    phone_number = StringField(
        'Phone #:', [validators.DataRequired(), validators.Length(min=0, max=16)])
    address = StringField('Address:', [
                          validators.DataRequired(), validators.Length(min=0, max=500)])
    est_volume = SelectField('Est load size:', choices=LOAD_SIZES_CHOICES)
    description = TextAreaField('Description/Notes:', [validators.Length(min=0, max=1000)])
    dt = DateField('Pick a Day', [validators.DataRequired()], format='%Y-%m-%d')
    time = SelectField('Select a time', choices=[('8:00', '8AM'), ('9:00', '9AM'), ('10:00', '10AM'),
                                                  ('11:00', '11AM'), ('12:00', 'Noon'), ('1:00', '1PM'),
                                                  ('2:00', '2PM'), ('3:00', '3PM'), ('4:00', '4PM'),
                                                  ('5:00', '5PM'), ('6:00', '6PM'), ('7:00', '7PM')])
    cost = StringField('Quoted Cost', [validators.Length(min=0, max=10)])
    submit = SubmitField()

    def validate_phone_number(form, field):
        if len(field.data) > 16:
            raise ValidationError('Invalid phone number.')
        try:
            input_number = phonenumbers.parse(field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
        except:
            input_number = phonenumbers.parse("+1"+field.data)
            if not (phonenumbers.is_valid_number(input_number)):
                raise ValidationError('Invalid phone number.')
