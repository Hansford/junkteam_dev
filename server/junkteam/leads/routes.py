from flask import render_template, url_for, request, flash, redirect, session
from junkteam.leads import bp
from junkteam.models import Lead, Quote
from junkteam import app, db
from junkteam.leads.forms import LeadForm, LOAD_SIZES_CHOICES
# from werkzeug.urls import url_parse
from flask_login import login_required

''' INTERNAL SUBDOMAIN BLUEPRINT '''

#Leads Dashboard - Loads in the leads
@bp.route('/', methods=['GET', 'POST'] )
@login_required
def leads():
    page = request.args.get('page', 1, type=int)
    leads = Lead.query.order_by(Lead.lead_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('leads.leads', page=leads.next_num) \
        if leads.has_next else None
    prev_url = url_for('leads.leads', page=leads.prev_num) \
        if leads.has_prev else None
    return render_template('leads/leads.html', title='Leads',
                           leads=leads.items,
                           next_url=next_url, prev_url=prev_url)

#Lead Summary  - Loads in the selected appointment 
@bp.route('/lead/<lead_id>', methods=['GET', 'POST'])
@login_required
def lead(lead_id):
    print(f"=====LEAD ID: {lead_id} ===========")
    lead = Lead.query.get(lead_id)
    if lead:                                     
        return render_template('leads/lead_summary.html', lead=lead)
    id = "89"
    id = int(lead_id)
    if id != 0:
        lead = Lead.query.get(id-1)
        flash("Last order")
        return redirect(url_for('leads.lead', lead_id=lead.lead_id))
    lead = Lead.query.get(id+1)
    return redirect(url_for('leads.lead', lead_id=lead.lead_id))

def availability(date, hour): 
    leads = Lead.query.filter_by(date=date).all()
    date_lead_times = {}
    if leads:
        for id, lead in enumerate(leads):
            date_lead_times[str(id)] = str(lead.time)

        for time in date_lead_times.values():
            print(time)
            if time == hour:
                print(f"Time Slot Taken at {hour}")
                return False
    return True


@bp.route('/create_lead', methods=['GET', 'POST'])
@login_required
def create_lead():
    form = LeadForm()
    if form.validate_on_submit():
        est_load_display = dict(LOAD_SIZES_CHOICES).get(form.est_volume.data)   
        time = form.time.data  
        date = form.dt.data 
        print(f"Testing time slot with time: {time} and date: {date} ")
        available = availability(date, time)
        if available:

            print("Timeslot available")
            lead = Lead(name=form.name.data, email=form.email.data, phone_number=form.phone_number.data,
                        address=form.address.data, est_volume=est_load_display, description=form.description.data, date=date, time=time)
            lead.cost = float(form.est_volume.data)
            db.session.add(lead)
            db.session.commit()
            print("TRYING TO FLASH")
            flash("Lead Created")
            return redirect(url_for('leads.leads'))
        flash("Time Taken")
        
    return render_template("leads/create_lead.html", form=form)




# edit lead
@bp.route('/edit_lead/<id>', methods=['GET', 'POST'])
@login_required
def edit_lead(id):
    lead = Lead.query.get(id)
    form = LeadForm()
    print(f"lead.est_volume: {lead.est_volume}")
    est_vol_dict = dict(LOAD_SIZES_CHOICES)
    for key, value in est_vol_dict.items():  
        if value == lead.est_volume:
            print(f"key: {key}")
            est_vol = key
    est_load_display = dict(LOAD_SIZES_CHOICES).get(form.est_volume.data)

    if form.validate_on_submit():
        lead.name=form.name.data
        lead.email=form.email.data
        lead.phone_number=form.phone_number.data
        lead.address=form.address.data
        lead.est_volume=est_load_display
        lead.description=form.description.data
        lead.date=form.dt.data
        lead.lead_id=id
        lead.userid=session["user_id"]
        lead.time=form.time.data
        lead.cost=form.est_volume.data
        db.session.commit()
        return redirect(url_for('leads.leads'))
    return render_template('leads/edit_lead.html', est_vol=est_vol, form=form, lead=lead)

# Delete Lead
@bp.route('/delete_lead/<id>')
@login_required
def delete_lead(id):
    Lead.query.filter_by(lead_id=id).delete()
    db.session.commit()
    return redirect(url_for('leads.leads'))

