from flask import Blueprint

''' INTERNAL SUBDOMAIN BLUEPRINT '''

bp = Blueprint('quotes', __name__)
from junkteam.quotes import routes
