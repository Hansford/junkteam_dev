from flask import render_template, url_for, request, redirect, session, flash, abort
from junkteam.models import Lead, Quote, Charge, Item, Items, Order
from junkteam.quotes import bp
from junkteam import app, db
# from werkzeug.urls import url_parse
from flask_login import login_required
from junkteam.quotes.forms import QuoteForm, LOAD_SIZES_CHOICES, Add_Charge_Item_Form


''' INTERNAL SUBDOMAIN BLUEPRINT '''

# Login page


#Quotes Dashboard - Loads in the current Quotes 
@bp.route('/', methods=['GET', 'POST'])
@login_required
def quotes():
    page = request.args.get('page', 1, type=int)
    quotes = Quote.query.order_by(Quote.quote_id.desc()).paginate(
        page, app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('quotes.quotes', page=quotes.next_num) \
        if quotes.has_next else None
    prev_url = url_for('quotes.quotes', page=quotes.prev_num) \
        if quotes.has_prev else None
    return render_template('quotes/quotes.html', title='Quotes',
                           quotes=quotes.items,
                           next_url=next_url, prev_url=prev_url)

#Quote Summary  - Loads in the selected Quote 
@bp.route('/quote/<quote_id>', methods=['GET', 'POST'])
@login_required
def quote(quote_id):

    quote = Quote.query.get(quote_id)
    if quote:
        charge = quote.charge
        items = Items.query.filter_by(charge_id=quote.charge.id).all()
        sub = quote.charge.get_subtotal() 
        tax = sub * 0.13
        total = sub * 1.13
        form = Add_Charge_Item_Form()
        if form.validate_on_submit():
            item_display = dict(LOAD_SIZES_CHOICES).get(form.new_item.data)
            item = Item.query.filter_by(lookup=item_display).first()
            items = Items.query.filter_by(charge_id=charge.id).all()
            item_to_add = Items(charge_id=charge.id, item_id = item.id, quantity = 1)
            print(f"Item to add: {item_to_add.item_id}  Items:{items}")

            for i in items:
                print(f"item ids? :  {i.item_id}")
                if item_to_add.item_id == i.item_id:
                    flash("Error")
                    return render_template('quotes/add_charge_item.html', form=form) 
            else:
                print("Trying to commit")
                db.session.add(item_to_add) 
                db.session.commit()
                update_cost(quote_id)
            return redirect(url_for('quotes.quote', quote_id=quote.quote_id))
        if quote.ordered:
            order = Order.query.filter_by(quote_id = quote_id).first()
            if not order:
                order = None
        else:
            order = None
        return render_template('quotes/quote.html', form=form, order=order, quote=quote, sub=sub, tax=tax, total=total, items=items)
    id = int(quote_id)
    if id != 0:
        quote = Quote.query.get(id-1)
        flash("Last Quote")
        return redirect(url_for('quotes.quote', quote_id=quote.quote_id))
    quote = Quote.query.get(id+1)
    return redirect(url_for('quotes.quote', quote_id=quote.quote_id))

# Create qute from Lead id
@bp.route('/create_quote/<id>', methods=['GET', 'POST'])
@login_required
def quote_from_lead(id):
    lead = Lead.query.get(id)
    form = QuoteForm()
    print(f"lead.est_volume: {lead.est_volume}")
    est_vol_dict = dict(LOAD_SIZES_CHOICES)
    for key, value in est_vol_dict.items():  
        if value == lead.est_volume:
            print(f"key: {key}")
            est_vol = key

    if form.validate_on_submit():
        est_load_display = dict(LOAD_SIZES_CHOICES).get(form.est_volume.data)
        print("Creating Quote")
        try:
            quote = Quote(name=form.name.data, email=form.email.data,
                        phone_number=form.phone_number.data,
                        address=form.address.data,
                        est_volume=est_load_display,
                        description=form.description.data, date=form.dt.data,
                        lead_id=id, userid=session["user_id"], time=form.time.data)
            db.session.add(quote)
            db.session.commit()
            print(f"New Quote ID: {quote.quote_id}")
            #create charge, add item, link to quote object and commit
            # MOVE TO A FUNCTION
            charge = Charge(quote_id=quote.quote_id)
            db.session.add(charge)
            db.session.commit()
            print(f"Lookup value: {est_load_display}")
            initial_item = Item.query.filter_by(lookup=est_load_display).first()
            if initial_item:
                print(f"Initial Item: {initial_item}")
            item_to_add = Items(charge_id=charge.id, item_id=initial_item.id, quantity = 1)
            db.session.add(item_to_add)
            db.session.commit()
            print(f"Quote ID for cost uodate: {quote.quote_id}")
            update_cost(quote.quote_id)
            
        except:
            print("Failed to create quote...")
        
        return redirect(url_for('quotes.quote', quote_id=quote.quote_id))
    return render_template('quotes/quote_from_lead.html', est_vol=est_vol, form=form, lead=lead)

# edit quotes
@bp.route('/edit_quote/<id>', methods=['GET', 'POST'])
@login_required
def edit_quote(id):
    quote = Quote.query.get(id)
    form = QuoteForm()
    print(f"Quote to edit: {quote}")
    if form.validate_on_submit():
        quote.name=form.name.data
        quote.email=form.email.data
        quote.phone_number=form.phone_number.data
        quote.address=form.address.data
        quote.description=form.description.data
        quote.date=form.dt.data
        quote.time=form.time.data
        db.session.commit()
        return redirect(url_for('quotes.quotes'))
    return render_template('quotes/edit_quote.html', form=form, quote=quote)




# Create Quote
@bp.route('/create_quote', methods=['GET', 'POST'])
@login_required
def create_quote():
    form = QuoteForm()
    
    if form.validate_on_submit():
        est_load_display = dict(LOAD_SIZES_CHOICES).get(form.est_volume.data)
        quote = Quote(name=form.name.data, email=form.email.data,
                      phone_number=form.phone_number.data,
                      address=form.address.data,
                      est_volume=est_load_display,
                      description=form.description.data, date=form.dt.data,
                      time=form.time.data)
        db.session.add(quote)
        db.session.commit()
        print(f"New Quote ID: {quote.quote_id}")
        #create charge, add item, link to quote object and commit
        # MOVE TO A FUNCTION
        charge = Charge(quote_id=quote.quote_id)
        db.session.add(charge)
        db.session.commit()
        print(f"Lookup value: {est_load_display}")
        initial_item = Item.query.filter_by(lookup=est_load_display).first()
        if initial_item:
            print(f"Initial Item: {initial_item}")
        item_to_add = Items(charge_id=charge.id, item_id=initial_item.id, quantity = 1)
        db.session.add(item_to_add)
        db.session.commit()
        print(f"Quote ID for cost uodate: {quote.quote_id}")
        update_cost(quote.quote_id)
     
        return redirect(url_for('quotes.quotes'))
    return render_template('quotes/create_quote.html', form=form)


def update_cost(id):
    quote = Quote.query.get(id)
    items = Items.query.filter_by(charge_id=quote.charge.id).all()
    sub = quote.charge.get_subtotal()
    quote.cost = sub
    db.session.commit()


# Delete Quote
@bp.route('/delete_quote/<id>')
@login_required
def delete_quote(id):
    quote = Quote.query.get_or_404(id)
    if quote.deleted:
        abort(404)
    quote.deleted = True
    db.session.commit()
    
    return redirect(url_for('quotes.quotes'))

@bp.route('/add_charge_item/<quote_id>', methods=['GET', 'POST'])
@login_required
def add_charge_item(quote_id):
    quote = Quote.query.get(quote_id)
    if quote.ordered:
        return redirect(url_for('quotes.quote', quote_id=quote.quote_id)) 
    charge = quote.charge
    form = Add_Charge_Item_Form()
    if form.validate_on_submit():
        item_display = dict(LOAD_SIZES_CHOICES).get(form.new_item.data)
        item = Item.query.filter_by(lookup=item_display).first()
        items = Items.query.filter_by(charge_id=charge.id).all()
        item_to_add = Items(charge_id=charge.id, item_id = item.id, quantity = form.quantity.data)
        print(f"Item to add: {item_to_add.item_id}  Items:{items}")

        for i in items:
            print(f"item ids? :  {i.item_id}")
            if item_to_add.item_id == i.item_id:
                flash("Error")
                return render_template('quotes/add_charge_item.html', form=form) 
        else:
            print("Trying to commit")
            db.session.add(item_to_add) 
            db.session.commit()
            update_cost(quote_id)
        return redirect(url_for('quotes.quote', quote_id=quote.quote_id))
    return render_template('quotes/add_charge_item.html', form=form)



@bp.route('/delete_charge_item')
@login_required
def delete_charge_item():
    quote_id = request.args.get('quote_id')
    item_id = int(request.args.get('item_id'))
    quote = Quote.query.get(quote_id)
    items = Items.query.filter_by(charge_id=quote.charge.id).all()
    print(f"Items: {items}")
    for item in items:
        if item.item.id == item_id:
            print(f"Item to Delete: {item.item}")
            del_items = db.session.query(Items).filter(Items.charge_id==quote.charge.id, Items.item_id==item_id).delete()
            print(f"Del: {del_items}")
            db.session.commit()
            update_cost(quote_id)
    print(f"Quote_id: {quote_id}")
    return redirect(url_for('quotes.quote', quote_id=quote.quote_id))

@bp.route('/decrement_charge_item')
@login_required
def decrement_charge_item():
    quote_id = request.args.get('quote_id')
    item_id = int(request.args.get('item_id'))
    quote = Quote.query.get(quote_id)
    items = Items.query.filter_by(charge_id=quote.charge.id).all()
    print(f"Items: {items}")
    for item in items:
        if item.item.id == item_id:
            print(f"Item to Delete: {item.item}")
            dec_item = db.session.query(Items).filter(Items.charge_id==quote.charge.id, Items.item_id==item_id).first()
            dec_item.quantity = item.quantity - 1
            db.session.commit()
            update_cost(quote_id)
    return redirect(url_for('quotes.quote', quote_id=quote.quote_id))

@bp.route('/increment_charge_item')
@login_required
def increment_charge_item():
    quote_id = request.args.get('quote_id')
    item_id = int(request.args.get('item_id'))
    quote = Quote.query.get(quote_id)
    items = Items.query.filter_by(charge_id=quote.charge.id).all()
    print(f"Items: {items}")
    for item in items:
        if item.item.id == item_id:
            print(f"Item to Delete: {item.item}")
            increment_item = db.session.query(Items).filter(Items.charge_id==quote.charge.id, Items.item_id==item_id).first()
            increment_item.quantity = item.quantity + 1
            db.session.commit()
            update_cost(quote_id)
    return redirect(url_for('quotes.quote', quote_id=quote.quote_id))