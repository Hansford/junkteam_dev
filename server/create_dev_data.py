#!/usr/bin/env python
import sys
from getpass import getpass
from junkteam import db, app, LOAD_SIZES_CHOICES
from junkteam.models import User, UserRoles, Role, Lead
from faker.providers import BaseProvider, date_time
from random import choice, randrange
from datetime import date


class FAKE_LEAD_DATA(BaseProvider):
    def fake_lead_data(self):
        cost, est_vol = choice(LOAD_SIZES_CHOICES)
        
        #get random time 
        time_raw = randrange(1, 13)
        time = f"{time_raw}:00"
        user_id = randrange(1, 4)
        return cost, est_vol, time, user_id


def create_roles(roles):

    try:
        if type(roles) == str:
            role = Role(name=role)
            db.session.add(role)
            print(f"Added Role: {role}")
            db.session.commit()
            return True

        if type(roles) == list:

            for role in roles:
                role = Role(name=role)
                db.session.add(role)
                print(f"Added Role: {role}")
                db.session.commit()
        if type(roles) == str:
            role = Role(name=role)
        
    except:
        print("Failure: Roles not created.")
        return False


def addRole(user_id, role):

    roles = Role.query.all()
    for role in roles:
        role_id = role.id
        print("Adding role {role} of ID {role_id}")
        try:
            user_role = UserRoles(user_id=user_id, role_id=role_id)
            db.session.add(user_role)
            db.session.commit()
            print("SUCCESS - USERS ROLE ADDED")
            return True
        except:
            return False


def create_user(username, email, password, role):
    print("creating user...")

    if username is None:
        username = input(f"Enter Username:")
        email = input(f"Enter {role} Email Address:")
        password = getpass()
        role = "USER"
        assert password == getpass('Password (again):')
    #Try to create the user
    usernametest = User.query.filter_by(username=username).first()
    if usernametest is None:

        print(f"User Data - username: {username}  role: {role}")
        user = User(username=username, email=email)
        print(f"User Object: {user}")
        db.session.add(user)
        print("User Added")
        user.set_password(password)
        print("password set")
        db.session.commit()
        print("User Created")

    else:
        print(f"Username {username} taken")
        return
    #try to assign a role
    print(f"Attempting to assign role {role} to {username}...")
    try:
        user = User.query.filter_by(username=username).first()
        user_id = user.id
        
        print(f"User Queried: {user.username}, Assinging Role: {role}")
        addRole(user_id, role)
        print(f"Role {role} added to User: {user.username}")
        

    except:
        print("Failure: Role not assigned.")
        return False
    
    print("SUCCESS - ADDED USER AND ASSIGNED ROLE")
    return True




def create_lead(fake):
    print("Getting fake lead data")
    fake_cost, fake_vol, fake_time, fake_user_id = fake.fake_lead_data()
    fake_date = fake.date_this_month(before_today=False, after_today=True)
    lead = Lead(name=fake.name(), email=fake.email(), phone_number=fake.phone_number(), address=fake.address(), est_volume=fake_vol, cost = fake_cost, description="Fake Lead", date=fake_date, time=fake_time, userid=fake_user_id)
    db.session.add(lead)
    db.session.commit()
    print("Lead Created")

    return True
def create_mock(data, num, fake):
    print(f"Trying to create {num} random {data}")
    for _ in range(num):
        if data == 'users':
            create_user(fake.first_name(), fake.email(), fake.password(), "TECHNICIAN")
        elif data == 'leads':
            create_lead(fake)
        else:
            create_order()
  
    return True


def mock_data(ops):
    from faker import Faker
    '''
        ops should be a list of mock options tuples
        ops = [('users', 5), ('leads', 300), (quotes, 100)]
    '''
    fake = Faker()
    fake.add_provider(FAKE_LEAD_DATA)
    fake.add_provider(date_time)
    with app.app_context():
        db.metadata.create_all(db.engine)
        for op in ops:
            data, num = op
            create_mock(data, num, fake)
                
        print("Data Mocked")
    return
        

def main():
    with app.app_context():
        db.metadata.create_all(db.engine)

        #Role Test
        roles_test = Role.query.get(1)
        if roles_test is None:
            print("Creating the Base Roles")
            create_roles(["ADMIN", "MANAGER", "TECHNICIAN"])
        else:
            print("Roles exist..")
        
        #Try to create Base Users
        user_test = User.query.get(1)
        if user_test is None:
            try:
                create_user("admin", "admin@junkteam.ca", "password", "ADMIN")
                print("Admin Created")
                create_user("manager", "manager@junkteam.ca", "password", "MANAGER")
                print("Manager Created")
                create_user("technician", "technician@junkteam.ca", "password", "TECHNICIAN")
                print("Base User Created")
                print("SUCCESS: BASE USERS CREATED")
            
            except:
                print("Epic Fail")
                sys.exit(1)


        return True
if __name__ == '__main__':
    sys.exit(main())
