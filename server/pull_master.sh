#!/bin/sh
#This is a simple bash script that will poll github for changes to your repo,
#if found pull them down, and then restart app instance

while true
do

cd ~/jtapp/junkteam
echo In JunkTeam
git fetch;
echo Fetched state
LOCAL=$(git rev-parse HEAD);
echo $LOCAL
REMOTE=$(git rev-parse @{u});
echo $REMOTE
#if our local revision id doesn't match the remote, we will need to pull the ch$

if [ $LOCAL = $REMOTE ]; then

   echo Up to date!
fi

if [ $LOCAL != $REMOTE ]; then
    #pull and merge changes
    echo Not up to date, pulling master
    git pull;
    #change back to home directory
    echo Pull Successful
    cd
    echo restarting JunkTeam service
    sudo systemctl restart junkteam;
fi
echo Exiting
sleep 2

exit
done

